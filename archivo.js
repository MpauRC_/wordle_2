const fs = require('fs');
const path = require('path');

const rutaArchivoUsuarios = path.join(__dirname, 'data/usuarios.json');
const rutaArchivoPalabras = path.join(__dirname, 'data/palabras.json');



//Función que se encarga de cargar las palabras
//del diccionario de la aplicación
const cargarPalabras = () => {
    if (fs.existsSync(rutaArchivoPalabras)){
        const file = fs.readFileSync(rutaArchivoPalabras);
        const { palabras } = JSON.parse(file);
        return palabras;
    }
}
const cargarUsuarios = () => {
    if (fs.existsSync(rutaArchivoUsuarios)){
        try {
            const file = fs.readFileSync(rutaArchivoUsuarios);
            return JSON.parse(file);
        } catch (error){
            return {
                usuarios: []
            }
        }
    }
}

const guardarUsuarios = (data) => {
    fs.writeFileSync(rutaArchivoUsuarios, JSON.stringify(data));

}

module.exports = {
    cargarPalabras,
    cargarUsuarios,
    guardarUsuarios
}