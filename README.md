
# INTEGRANTES:

    ⁕ Valentina Aldana Tabares
    ⁕ Mariapaula Rodríguez Castañeda

# PROYECTO A REALIZAR

Se pondrá en marcha el inicio del un proyecto basado en Wordles con indicaciones específicas, inicialmente por consola.

 ⁕ Crear cuenta
 ⁕ Inicio de sesión
 ⁕ Cerrar sesión

 Una vez ingresada la cuenta se dispondrá a tener las siguientes opciones para jugar:

    ⁕ Crear un nuevo juego
    ⁕ Consultar estadísticas
    ⁕ Salir del juego