const chalk = require('chalk');
const { cargarPalabras, cargarUsuarios, guardarUsuarios } = require('./archivo.js');
const { inputPalabra } = require('./input.js');

//Almacena el número de letras con las que se
// va a jugar el juego 
const letras = 5;
//Almacena el número máximo de intentos permitidos
//en el juego
const intentos = 6;
//Arreglo que almacena las palabras cargadas desde el archivo.js
const diccionario = cargarPalabras();
//Función que se encarga de todo el inicio de funcionamiento
//del juego
const iniciarJuego = async (indexUser) => {
    const aleatorio = Math.floor(Math.random()*diccionario.length);
    const palabraGenerada = diccionario[aleatorio].name;
    const intentos = [];
    let numIntentos = 0;
    let aciertos = 0;

//While que no permite más de cinco intentos
// y que valida que las palabras ingresadas y correctas sean 5
    while ( numIntentos < intentos && aciertos !== letras) {
        //Se almacena la palabra ingresada por el jugador 
        //y se convierte a mayúsculas
        let { palabraIngresada } = await inputPalabra();
        palabraIngresada = palabraIngresada.toUpperCase();

        // Se valida que la palabra ingresada contenga
        //cinco letras
        if (palabraIngresada.length < letras){
            console.log('No hay suficientes letras para una palabra');
            continue;
        }

        const found = diccionario.find(p => palabraIngresada == p.name);

        //Se valida que la palabra esté en el diccionario
        if (!found) {
            console.log('La palabra no está registrada en el diccionario ');
            continue;
        }

        //Arreglo que almacena la información correspondiente
        //por cada intento
        const intento = [];
        // Se reinicia el número de letras acertadas
        aciertos = 0;

        for(let i = 0; i < letras; i++){
            const letraIngresada = palabraIngresada.charAt(i);
            // 1) Se valida que la letra en la posicion i
            //está en  la posicion correcta
            // 2) Se valida que la palabra que se generó incluya
            //la letra que se ingresó
            if (palabraGenerada.includes(letraIngresada)) {
                //Devuelve el caracter en la posicion i 
                const letraGenerada = palabraGenerada.charAt(i);
                // Verificación de marcación verde 
                if (letraGenerada === letraIngresada){
                    intento.push({
                        // 1) Se valida que el caracter esté en la palabra generada
                        LaPalabraEsta: true,
                        // 2) Se valida que el caracter esté en la posición correcta
                        EstaEnLaPosicion: true,
                        // 3) Se pinta el caracter del color verde
                        letraOriginal: letraIngresada,
                        color: chalk.green(letraIngresada)
                    });
                    aciertos++;
                // Verificación de marcación magenta
                } else {
                    intento.push({
                        // 1) Se valida que el caracter esté en la palabra generada
                        LaPalabraEsta: true,
                        // 2) Se valida que el caracter esté en la posición correcta
                        EstaEnLaPosicion: false,
                        // 3) Se pinta el caracter del color magenta
                        letraOriginal: letraIngresada,
                        color: chalk.magenta(letraIngresada)
                    });
                    aciertos = 0;
                //Verificación de marcación magenta
                } 
        //verficación de marcación roja
            } else {
                intento.push({
                    // 1) Se valida que el caracter esté en la palabra generada
                    LaPalabraEsta: false,
                    // 2) Se valida que el caracter esté en la posición correcta
                    EstaEnLaPosicion: false,
                    // 3) Se pinta el caracter del color rojo
                    LetraOriginal: letraIngresada,
                    color: chalk.red(letraIngresada)
                });
                aciertos = 0;
            }
        }
            intentos.push(intento);
            numIntentos++;

            mostrarTablero(intentos);
            mostrarTeclado(intentos);
    }
    
    //Valida los intentos
    // Verfica si completo las 5 letras correctamente
    if (aciertos === letras){
        console.log(`Felicidades. Ha completado la palabra en ${ numIntentos } intento(s) 😊.` );
    // Verfica si no se completaron las 5 letras correctamente
    }else{
        console.log(`Has fallado. No te preocupes, en la próxima será 😎.`);
        console.log(`La palabra del día es: ${ palabraGenerada }`);
    }  
    
    guardarEstadisticas(numIntentos, indexUser);
}

//Función encargada de mostrar el tablero de intentos en pantalla 
const mostrarTablero = (tablero) => {
    let i = 0;
    //Muestra las cajas, cada una con su respectiva letra
    for (const intento of tablero) {
        let fila = '';
        for (const letra of intento){
            fila = fila + `[${ letra.color }]`;
        }
        console.log('          ', fila);
        i++;
    }

    
    while (i < intentos){
        let fila = '[ ][ ][ ][ ][ ]';
        console.log('          ', fila);
        i++;
    }
}

//Función encargada de mostrar y colorear el teclado según
//las letras indicadas
const mostrarTeclado = (tablero) => {
    //Arreglo que contiene los elementos del teclado
    const teclado = [
        [{ letra: ' ', valor: ' ' }, { letra: 'Q', valor: 'Q'}, { letra: 'W', valor: 'W' }, { letra: 'E', valor: 'E' }, { letra: 'R', valor: 'R' }, { letra: 'T', valor: 'T' }, { letra: ' ', valor: ' '}, { letra: 'Y', valor: 'Y'}, { letra: 'U', valor: 'U'}, { letra: 'I', valor: 'I'}, { letra: 'O', valor: 'O'}, { letra: 'P', valor: 'P'}], 
        [{ letra: ' ', valor: ' '}, { letra: 'A', valor: 'A'}, { letra: 'S', valor: 'S'}, { letra: 'D', valor: 'D'}, { letra: 'F', valor: 'F'}, { letra: 'G', valor: 'G'}, { letra: ' ', valor: ' '}, { letra: 'H', valor: 'H'}, {letra: 'J', valor: 'J'}, {letra: 'K', valor: 'K'}, { letra: 'L', valor: 'L'}, { letra: 'Ñ', valor: 'Ñ'}],
        [{ letra: 'ENVIAR', valor: 'ENVIAR'}, { letra: 'Z', valor: 'Z'}, { letra: 'X', valor: 'X'}, { letra: 'C', valor: 'C'}, { letra: 'V', valor: 'V'},{ letra: 'B', valor: 'B'}, { letra: 'N', valor: 'N'}, { letra: 'M', valor: 'M'}, { letra: 'BORRAR', valor: 'BORRAR'}],
    ];

    //Se encarga de colorear las letras del teclado por cada
    //intento que haya hecho el jugador
    for (const intento of tablero) {
        for (const letra of intento) {
            for (const fila of intento){
                fila.foreach((l, i) => {
                    if (l.letra === letra.letra) {
                        fila[i].valor = letra.color;
                    }
                });
            }
        }
    }

    //Muestra en pantalla el teclado
    for (const fila of teclado) {
        let mostrar = '';
        for (const letra of fila){
            mostrar = mostrar + ` ${letra.valor}`
        }
        console.log(mostrar);
    }

    console.log();
}

const consultarEstadisticas = (indexUser) => {

    const { usuarios } = cargarUsuarios();
    const usuario = usuarios[indexUser];
    const estadisticas = usuario.estadisticas;
    const intentos = estadisticas.intentos;
    const distribucion = [];

    //Se define si hay estadísticas en dependencia de las partidas
    if (estadisticas.jugados === 0) {
        console.log('No hay estadísticas para mostrar');
        return;
    }

    //Proceso para calcular las estadisticas correspondientes

    for (const intento of intentos){
        if (intento.intento === 6) {
            if (intento.completados === 0) {
                distribucion.push({
                    label: 'X: ', 
                    content: '(0%)'
                });
            }else{
                const numCajas = Math.round(intento.completados / estadisticas.jugados * 100);
                let content = '';

                for (let i = 0; i < numCajas; i++){
                    content = content + `[${ chalk.bgWhite('XX')}]`;
                }

                content  = content + `(${ (intento.completados / estadisticas.jugados * 100).toFixed(2) }%)`
                distribucion.push({
                    label: 'X: ', 
                    content: content
                });
            }
        }else{
            if(intento.completados === 0){
                distribucion.push({
                    label: `${ intento.intento }:`,
                    content: '(0%)'
                });
            }else{
                const numCajas = Math.round(intento.completados / estadisticas.jugados * 10 );
                let content = '';

                for (let i = 0; i < numCajas; i++){
                    content = content + `[${ chalk.bgWhite('XX') }]`;
                }

                content = content + `(${ (intento.completados / estadisticas.jugados * 100).toFixed(2) }%)`
                distribucion.push({
                    label: `${ intento.intento }:`,
                    content: content
                });

            }
        }
    }
    console.log(`${ estadisticas.jugados } Jugadas                        ${ ((estadisticas.ganados / estadisticas.jugados) * 100).toFixed(2) }% Victorias`);

    for (const intento of distribucion) {
        console.log(intento.label, intento.content);
    }
}

const guardarEstadisticas = (numIntentos, indexUser) => {
    const { usuarios } = cargarUsuarios();
    const usuario = usuarios[indexUser];

    if (numIntentos < intentos){
        usuario.estadisticas.ganados++;
    }
    usuario.estadisticas.jugados++;
    usuario.estadisticas.intentos[numIntentos -1].completados++;
    usuarios[indexUser] = usuario;

    guardarUsuarios({
        usuarios: usuarios
    });
}

module.exports = {
    iniciarJuego,
    consultarEstadisticas
}
